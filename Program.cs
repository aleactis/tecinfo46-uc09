﻿using System;

namespace Tec46_Aula08
{
    class Program
    {
        static string temp;
        static int opcao = 1;

        static void Main(string[] args)
        {
            while (opcao >= 1 && opcao <= 17)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("----------------------------");
                Console.WriteLine("Digite o número do exercício");
                Console.WriteLine("----------------------------");
                Console.ResetColor();
                Console.WriteLine("1 - Exibir uma sequência de números na ordem inversa");
                Console.WriteLine("2 - Exibir números perfeitos");
                Console.WriteLine("3 - Exibir números primos");
                Console.WriteLine("4 - Verifica idade do eleitor para votar em eleições brasileiras");
                Console.WriteLine("5 - Exibe múltiplos de 3 até 100");
                Console.WriteLine("6 - Exibe múltiplos de 3 ou 4 até 30");
                Console.WriteLine("7 - Exibe fatorial de 1 até 10");
                Console.WriteLine("8 - Fibonacci até passar de 100");
                Console.WriteLine("9 - Exibir mútiplos de cada linha até chegar no valor determinado de 'n'");
                Console.WriteLine("10 - Exibir uma forma de pirâmide com asteriscos baseado no nº de linhas fornecido");
                Console.WriteLine("11 - Digitar nome, número e bairro");
                Console.WriteLine("12 - Tipos de conversão");
                Console.WriteLine("13 - Salário do funcionário no mês");
                Console.WriteLine("14 - Somar elementos em um array");
                Console.WriteLine("15 - Tabuada de um número A por um número B");
                Console.WriteLine("16 - Calcular média de 10 valores com array");
                Console.WriteLine("17 - Calculadora de áreas");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("18 - Sair");
                Console.ResetColor();

                Console.Write("Opção desejada: ");
                temp = Console.ReadLine();
                
                while (Int32.TryParse(temp, out opcao) == false ||
                    opcao < 1 || opcao > 18)
                {
                    Console.WriteLine("Opção inválida. Digite novamente: ");
                    temp = Console.ReadLine();
                }

                switch (opcao) 
                {
                    case 1:
                        Ex01(); //Exibir uma sequência de números na ordem inversa
                        break;
                    case 2:
                        Ex02(); //Exibir números perfeitos
                        break;
                    case 3:
                        Ex03(); //Exibir números primos
                        break;
                    case 4:
                        Ex04(); //Verifica idade do eleitor para votar em eleições brasileiras
                        break;
                    case 5:
                        Ex05(); //Exibe múltiplos de 3 até 100
                        break;
                    case 6:
                        Ex06(); //Exibe múltiplos de 3 ou 4 até 30
                        break;
                    case 7:
                        Ex07(); //Exibe fatorial
                        break;
                    case 8:
                        Ex08(); //Fibonacci até passar de 100
                        break;
                    case 9:
                        Ex09(); //Exibir mútiplos de cada linha até chegar no valor determinado de 'n'
                        break;
                    case 10:
                        Ex10(); //Exibir uma forma de pirâmide com asteriscos baseado no nº de linhas fornecido
                        break;
                    case 11:
                        Ex11(); //Digitar nome, cidade e bairro
                        break;
                    case 12:
                        Ex12(); //Exemplo básico de conversão explícita em C#
                        break;
                    case 13:
                        Ex13(); //Jornada de trabalho de um func.
                        break;
                    case 14:
                        Ex14(); //Somar elementos em um array
                        break;
                    case 15:
                        Ex15(); //Tabuada de um número A por um número B
                        break;
                    case 16:
                        Ex16(); //Calcular média de 10 valores com array
                        break;                                                                   
                    case 17:
                        Ex17(); //Calculadora de áreas
                        break;
                    case 18:
                        Console.WriteLine("Saindo...");
                        break;
                    default:
                        Console.WriteLine("Exercício inválido");
                        break;
                }
            }
        } //final do método Main()

        static void Ex01()
        {
            int num, r, sum = 0, t;

            Console.Write("\n\n");
            Console.Write("Exibir um sequência de números na ordem inversa:\n");
            Console.Write("--------------------------------------");
            Console.Write("\n\n");


            Console.Write("Digite uma sequência de números: ");
            num = Convert.ToInt32(Console.ReadLine());

            for (t = num; t != 0; t = t / 10)
            {
                r = t % 10;
                sum = sum * 10 + r;
            }
            Console.Write("Os números em seequência inversa são : {0} \n", sum);
        }
        
        static void Ex02()
        {
            /*Um número se diz perfeito se é igual à soma de seus divisores próprios. Divisores próprios de um número positivo N são todos os divisores inteiros positivos de N exceto o próprio N.
            Por exemplo, o número 6, seus divisores próprios são 1, 2 e 3, cuja soma é igual à 6. 1 + 2 + 3 = 6
            Outro exemplo é o número 28, cujos divisores próprios são 1, 2, 4, 7 e 14, e a soma dos seus divisores próprios é 28.
            1 + 2 + 4 + 7 + 14 = 28*/
            int n, i, sum;
            int mn, mx;

            Console.Write("\n\n");
            Console.Write("Localiza números perfeitos em um intervalo dado:\n");
            Console.Write("------------------------------------------------------");
            Console.Write("\n\n");

            Console.Write("Digite um número do intervalo inicial : ");
            mn = Convert.ToInt32(Console.ReadLine());
            Console.Write("Digite um número do intervalo final : ");
            mx = Convert.ToInt32(Console.ReadLine());
            Console.Write("Os números perfeitos dentro do intervalo digitado são : ");
            for (n = mn; n <= mx; n++)
            {
                i = 1;
                sum = 0;
                while (i < n)
                {
                    if (n % i == 0)
                        sum = sum + i;
                    i++;
                }
                if (sum == n)
                    Console.Write("{0} ", n);
            }
            Console.Write("\n");
        }
        
        static void Ex03()
        {
            int num, i, ctr = 0;

            Console.Write("\n\n");
            Console.Write("Verifica se um número dado é primo:\n");
            Console.Write("-----------------------------------------------");
            Console.Write("\n\n");

            Console.Write("Digite um número: ");
            num = Convert.ToInt32(Console.ReadLine());
            for (i = 2; i <= num / 2; i++)
            {
                if (num % i == 0)
                {
                    ctr++;
                    break;
                }
            }
            if (ctr == 0 && num != 1)
                Console.Write("{0} é um número primo.\n", num);
            else
                Console.Write("{0} não é um número primo\n", num);
        }
        
        static void Ex04() {

            int idade = 16;
            bool brasileira = true;

            // se for brasileiro e tiver mais de 16 anos, pode votar
            if (idade >= 16 && brasileira)
                Console.WriteLine("Você pode votar");
            else // caso contrário não pode votar
                Console.WriteLine("Você não pode Votar");
        }
        
        static void Ex05()
        {
            for (int i = 1; i <= 100; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine("Múltiplos de 3 até 100: " + i);
                }
            }

            //Segunda forma com apenas 2 linhas
            //for (int i = 3; i <= 100; i += 3)
            //    Console.WriteLine("Múltiplos de 3 até 100: " + i);

            //Terceira forma
            //for (int i = 3; i <= 100; i = i + 3)
            //    Console.WriteLine("Múltiplos de 3 até 100: " + i);
        }
        
        static void Ex06()
        {
            for (int i = 0; i <= 30; i++)
            {
                if (i % 3 == 0 || i % 4 == 0)
                {
                    Console.WriteLine("Número: " + i);
                }
            }
        }

        static void Ex07()
        {
            int fatorial = 1;
            for (int n = 1; n <= 10; n++)
            {
                fatorial = fatorial * n;
                Console.WriteLine("Fatorial de " + n + " é: " + fatorial);
            }
        }

        static void Ex08()
        {
            int ant = 1;
            for (int cont = 0; cont < 100;)
            {
                Console.WriteLine(cont + " ");
                cont += ant;
                Console.WriteLine(ant + " ");
                ant += cont;
            }
        }

        static void Ex09()
        {
            // O programa imprime os mútiplos de cada linha até chegar no valor determinado de 'n'.
            // O primeiro for representa a quantidade de "linhas"
            // o segundo for representa a quantidade de "colunas"
            // O for de dentro que representa as "colunas", executa mais de 1 vez por conta da validação "j <= i"
            // Lembre-se que quando sair do for das "colunas" e entrar novamente o j assume o valor 1, ou seja,
            // ele é reinicializado, enquanto o i do for externo está sendo iterado, então ele sempre vai 
            // executar mais um vez...
            int n = 4; // número de linhas que queremos imprimir   
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(i * j + " ");
                }
                Console.WriteLine("");
            }
        }

       static void Ex10()
        {
            int i, j, spc, rows, k;

            Console.Write("\n\n");
            Console.Write("Exibir um padrão como uma pirâmide com asteriscos:\n");
            Console.Write("-------------------------------------------------");
            Console.Write("\n\n");

            Console.Write("Informe o número de linhas : ");
            rows = Convert.ToInt32(Console.ReadLine());
            spc = rows + 4 - 1;
            for (i = 1; i <= rows; i++)
            {
                for (k = spc; k >= 1; k--)
                {
                    Console.Write(" ");
                }

                for (j = 1; j <= i; j++)
                    Console.Write("* ");
                Console.Write("\n");
                spc--;
            }
        }

        //Método/Função criada para o exercício 1
        static void Ex11() 
        {
            string nome, cidade, bairro;

            //C# é Case-Sensitive => Diferencia maiúsculo de minúsculo
            Console.Write("Digite seu nome: ");
            nome = Console.ReadLine();

            Console.Write("Digite a sua cidade: ");
            cidade = Console.ReadLine();

            Console.Write("Digite o seu bairro: ");
            bairro = Console.ReadLine();
            
            Console.WriteLine("A pessoa " + nome + " mora em " + cidade 
                + " no bairro " + bairro);
        }

        static void Ex12() 
        {
            // Programa que exercita tipos de conversão implícita e explícita

            //Type Conversion => Tipos de conversão
            // 2 tipos de conversão: 
                // Implícita: Realizada pelo próprio C#
                // Explícita: Fica a cargo do programador
            
            //Declarando e inicializando a variável d
            double d = 1234.56;

            //Declarando a variável
            int i;

            //Casting
            i = (int)d;
            
            //Exibe a informação na saída padrão
            Console.WriteLine(i);
        }

        static void Ex13() 
        {
            // A jornada de trabalho semanal de um funcionário é de
            // 40 horas. O funcionário que trabalhar mais de 40
            // horas receberá hora extra, cujo cálculo é o valor da
            // hora regular com um acréscimo de 50%.
            // Escreva um Programa em C# que leia o número de
            // horas trabalhadas em um mês, o salário por hora e
            // escreva o salário total do funcionário, que deverá ser
            // acrescido das horas extras caso tenham sido
            // trabalhadas. (considere que o mês possua 4 semanas exatas)

            double salarioNormal, salarioPorHora, salarioTotal, valorHorasExtras, salarioExtra;
            int quantHorasExtras, quantHorasTrabalhadasMes;

            Console.Write("Quantas horas você trabalhou no mês: ");
            
            quantHorasTrabalhadasMes = Convert.ToInt32(Console.ReadLine());

            Console.Write("Digite seu salário por hora: ");
            salarioPorHora = Convert.ToDouble(Console.ReadLine());

            //Cálculo
            if (quantHorasTrabalhadasMes <= 160)
            {
                salarioNormal = salarioPorHora * quantHorasTrabalhadasMes;
                salarioExtra = 0;
            } else
            {
                //Calcula o salário normal que é referência a 160hs - 40 hs * 4 semanas
                salarioNormal = salarioPorHora * 160;
                //Calculando as horas extras
                quantHorasExtras = quantHorasTrabalhadasMes - 160;
                //Valor normal + 50%
                valorHorasExtras = salarioPorHora + (salarioPorHora * 50) / 100;
                //Valor total em horas extras
                salarioExtra = valorHorasExtras * quantHorasExtras;
            }

            //Cálculo do salário total
            salarioTotal = salarioNormal + salarioExtra;

            Console.WriteLine($"Seu salário total no mês foi de: {salarioTotal:c2}");
        }

        static void Ex14()
        {
            int n, soma = 0;

            Console.WriteLine("\n\nSomar todos os elementos de um array:\n");
            Console.WriteLine("----------------------------------------\n");

            Console.Write("Informe o número de elementos em seu array: ");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"Digite {n} elementos em seu array: ");

            //Passa o número de elementos de um array para a função, e ela retorna a soma desses elementos
            soma = SomarElmenteos(n);

            Console.Write($"\n\nA soma dos elementos do array é: {soma} \n\n");
            Console.ReadKey();
        }

        private static int SomarElmenteos(int numElementos)
        {
            int[] arr = new int[100];
            int soma = 0;


            for (int i = 0; i < numElementos; i++)
            {
                Console.Write($"Elemento {i}: ");
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < numElementos; i++)
            {
                soma += arr[1];
            }

            return soma;
        }

        private static void Ex15()
        {
            int a, b, res;

            Console.Write("Digite um número: ");
            a = Int32.Parse(Console.ReadLine());

            Console.Write("Digite outro número: ");
            b = Int32.Parse(Console.ReadLine());

            //Escreva um Programa em C# que receba um
            //número A e um número B, e escreva a
            //tabuada do número A até o limite definido
            //no número B.
            for (int i = 1; i <= b; i++)
            {
                res = a * i;
                Console.WriteLine(res);
            }
            Console.ReadKey();
        }

        private static void Ex16()
        {
            int soma=0, elementos=10;
            int[] valores = new int[elementos];

            for (int i = 0; i < elementos; i++)
            {
                Console.Write($"Digite o elemento {i}: ");
                valores[i] = Convert.ToInt32(Console.ReadLine());
                soma += valores[i];
            }
            
            Console.WriteLine($"A média dos {elementos} elementos é: {soma/elementos}");
            Console.ReadKey();
        }

        static void Ex17()
        {
            string texto;

            Console.WriteLine("Bem vindo a calculadora de áreas!!!");
            Console.WriteLine("A seguir escolha qual figura deseja, digitando o número correspondente a figura correspondente.");
            Console.WriteLine("1 ==> Quadrado");
            Console.WriteLine("2 ==> Triângulo");
            Console.WriteLine("3 ==> Circunferência");
            string escolha = Console.ReadLine();
            int opcao;
            if (!int.TryParse(escolha, out opcao))
            {
                Console.Write("Opção inválida");
                return;
            }

            switch (opcao)
            {
                case 1:
                    double lado;
                    Console.WriteLine("Digite o valor do lado do quadrado: ");
                    texto = Console.ReadLine();
                    if (!double.TryParse(texto, out lado))
                    {
                        Console.Write("Número inválido");
                        return;
                    }
                    lado = lado * lado;
                    Console.WriteLine($"O valor da área do quadrado é: {lado}");
                    break;
                case 2:
                    double base_t, altura, resultado;
                    Console.WriteLine("Digite o valor da base do triângulo: ");
                    texto = Console.ReadLine();
                    if (!double.TryParse(texto, out base_t))
                    {
                        Console.Write("Número inválido");
                        return;
                    }
                    Console.WriteLine("Digite o valor da altura do triângulo: ");
                    texto = Console.ReadLine();
                    if (!double.TryParse(texto, out altura))
                    {
                        Console.Write("Número inválido");
                        return;
                    }
                    resultado = base_t * altura / 2;
                    Console.WriteLine($"O valor da área do triângulo é: {resultado}");
                    break;
                case 3:
                    double raio, area;
                    Console.WriteLine("Digite o valor do raio da circunferência: ");
                    texto = Console.ReadLine();
                    if (!double.TryParse(texto, out raio))
                    {
                        Console.WriteLine("Número inválido");
                        return;
                    }
                    area = Math.PI * (raio * raio);
                    Console.WriteLine($"O valor da área da circunferência é: {area}");
                    break;
            }
        }   
    }
}
